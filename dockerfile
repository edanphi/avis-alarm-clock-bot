# Use an official Python runtime as the base image
FROM python:3.12

# Set the working directory in the container
WORKDIR /app

# Clone the Git repository into the container
RUN git clone https://gitlab.com/edanphi/avis-alarm-clock-bot.git .

# Copy the env variables into the container
COPY .env .

# Install any needed dependencies specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Run script.py when the container launches
CMD ["python", "-u", "avis_alarm_bot.py"]
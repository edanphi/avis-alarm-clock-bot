import discord
import os
import json
import asyncio
import discord
from discord.ext import commands
from datetime import datetime, timedelta

from dotenv import load_dotenv

load_dotenv()

TOKEN = os.getenv('BOT_TOKEN')
MINUTES_OFFSET = os.getenv('MINUTES_OFFSET')
MESSAGES = os.getenv('MESSAGES')

messages_list = json.loads(MESSAGES)

intents = discord.Intents.default()
bot = commands.Bot(command_prefix='!', intents=intents)

scheduled_tasks = {}

async def send_periodic_dm(user):
    while True:
        now = datetime.now()
        next_reminder_time = now + timedelta(hours=0, minutes=(int(MINUTES_OFFSET) - now.minute) % 60, seconds=-now.second, microseconds=-now.microsecond)
        time_until_next_reminder = (next_reminder_time - now).total_seconds()
        await asyncio.sleep(time_until_next_reminder)

        print("sending messages to: " + str(user))
        for message in messages_list:
            # Send DM to the user
            await user.send(message)
        print("done sending messages to: " + str(user))
        await asyncio.sleep(60)

@bot.tree.command()
async def start_reminders(interaction):
    # Check if the user already has a scheduled task
    if interaction.user.id in scheduled_tasks:
        await interaction.response.send_message("You already have reminders scheduled.", ephemeral=True)
    else:
        # Start the reminder task for the user
        scheduled_tasks[interaction.user.id] = bot.loop.create_task(send_periodic_dm(interaction.user))
        await interaction.response.send_message("Hourly reminders started. Use `!stop_reminders` to stop.", ephemeral=True)
        print(str(interaction.user) + " has enabled hourly reminders")

@bot.tree.command()
async def stop_reminders(interaction):
    # Check if the user has a scheduled task
    if interaction.user.id in scheduled_tasks:
        # Cancel the task
        scheduled_tasks[interaction.user.id].cancel()
        del scheduled_tasks[interaction.user.id]
        await interaction.response.send_message("Hourly reminders stopped.", ephemeral=True)
        print(str(interaction.user) + " has disabled hourly reminders")
    else:
        await interaction.response.send_message("You don't have any reminders scheduled.", ephemeral=True)

@bot.event
async def on_ready():
    print(f'We have logged in as {bot.user}')
    try:
        synced = await bot.tree.sync()
        print(f"Synced {len(synced)} command(s)")
    except Exception as e:
        print(e)

bot.run(TOKEN)